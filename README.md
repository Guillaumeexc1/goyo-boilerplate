# Boilerplate T3 Goyo

## Stack

- [T3 Stack](https://github.com/t3-oss/create-t3-app) - Interactive CLI to quickly set up an opinionated, full-stack, typesafe Next.js project.
  - [Next.js](https://nextjs.org) - A React framework with hybrid static & server rendering, and route pre-fetching, etc.
  - [tRPC](https://trpc.io) - tRPC allows you to easily build & consume fully typesafe APIs, without schemas or code generation.
  - [Tailwind CSS](https://tailwindcss.com) - A utility-first CSS framework that can be composed to build any design, directly in your markup. Support dark and light theming.
  - [TypeScript](https://typescriptlang.org) - TypeScript is a strongly typed programming language that builds on JavaScript, giving you better tooling at any scale.
  - [Prisma](https://prisma.io) - An ORM designed to build intuitive Database Client for TypeScript and Node.js
  - [NextAuth.js](https://next-auth.js.org) - NextAuth.js is a complete open-source authentication solution for Next.js applications.
- [Vercel](https://vercel.com/) - A hosting solution that works best with Next.js. Directly linked to the gitlab repo of the project.
- [Railway](https://railway.app/) - Fast and easy to setup solution to host your postgresql database for free.
- [Prettier](https://prettier.io/) - Structure the code in order to have consistent syntax.
- [Jotai](https://jotai.org/) - Global state management library. Jotai takes a bottom-up approach to React state management with an atomic model inspired by Recoil

### Optional

- [Zustand](https://github.com/pmndrs/zustand) - For more complex state handling situation in chich you want to create for functionnality than just a setHook.

### Setup

1.  Clone the project
2.  Create your custom database
    1. On [Railway](https://railway.app/) - Easy to setup
    2. Your custom Relational database of choice ( Postgresql, mysql, ...)
3.  Adding Google provider to enable Signin [Nextauth Complete Tutorial #7 Add Google provider](https://www.youtube.com/watch?v=QXgFaHEuJOE)
4.  Create a `.env` file and fill it with your variables, it should look somewhat similar to that :
    ![EnvFile](doc/envFile.png)

### Local commands

`yarn install` - To add the node_modules

`npx prisma migrate dev --name init` - To populate your database (Creating tables)

`npx prisma studio` - To see your tables

`yarn dev` - To start the project
