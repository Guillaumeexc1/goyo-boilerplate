import { useEffect, useState } from 'react'

import { CustomHeader } from '../components'
import { NextPage } from 'next'
import { useRouter } from 'next/router'

const ErrorPage: NextPage = () => {
  const [title, setTitle] = useState('Default Title')
  const [desc, setDesc] = useState('Default Description')

  const router = useRouter()

  useEffect(() => {
    setTitle(router.query.title as string)
    setDesc(router.query.desc as string)
  }, [router.query])

  return (
    <div>
      <CustomHeader />
      <main className="container mx-auto flex flex-col items-center justify-center min-h-screen p-4 gap-10 text-center">
        <h1 className="text-5xl md:text-[5rem] leading-normal font-extrabold">
          Error while accessing the content
        </h1>
        <p className="text-xl font-bold">{title}</p>
        <p className="text-lg italic">{desc}</p>
      </main>
    </div>
  )
}

export default ErrorPage
