import { FC, useState } from 'react'

import Image from 'next/image'
import { darkMode } from '../pages/_app'
import { useAtom } from 'jotai'

/* eslint-disable @next/next/no-html-link-for-pages */
const CustomHeader: FC = () => {
  const [isOpen, setIsOpen] = useState(false)
  const [isDarkMode, setDarkMode] = useAtom(darkMode)

  const customToggleDarkMode = () => {
    setDarkMode(!isDarkMode)
  }

  return (
    <div className={`${isDarkMode && 'dark'}`}>
      <nav className="shadow bg-primary-200">
        <div className="container px-6 py-4 mx-auto">
          <div className="lg:flex lg:items-center">
            <div className="flex items-center justify-between">
              <div>
                <a
                  className="text-2xl font-bold text-black transition-colors duration-300 transform lg:text-3xl "
                  href="#"
                >
                  Sarah Jane
                </a>
              </div>

              <div className="flex lg:hidden">
                <button
                  onClick={() => setIsOpen(!isOpen)}
                  type="button"
                  className=" focus:outline-none"
                  aria-label="toggle menu"
                >
                  {!isOpen ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth="2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4 8h16M4 16h16"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth="2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  )}
                </button>
              </div>
            </div>

            <div
              className={
                isOpen
                  ? ' bg-primary-200 translate-x-0 opacity-100 absolute inset-x-0 z-20 flex-1 w-full px-6 py-4 transition-all duration-300 ease-in-out  lg:mt-0 lg:p-0 lg:top-0 lg:relative lg:bg-transparent lg:w-auto lg:opacity-100 lg:translate-x-0 lg:flex lg:items-center lg:justify-between'
                  : ' opacity-0 -translate-x-full absolute inset-x-0 z-20 flex-1 w-full px-6 py-4 transition-all duration-300 ease-in-out  lg:mt-0 lg:p-0 lg:top-0 lg:relative lg:bg-transparent lg:w-auto lg:opacity-100 lg:translate-x-0 lg:flex lg:items-center lg:justify-between'
              }
            >
              <div className="flex flex-col capitalize lg:flex lg:px-16 lg:-mx-4 lg:flex-row lg:items-center">
                <a
                  href="#"
                  className="mt-2 text-black duration-300 transform lg:mt-0 lg:mx-4 motion-safe:hover:scale-105"
                >
                  Nouveauté
                </a>
                <a
                  href="#"
                  className="mt-2 text-black duration-300 transform lg:mt-0 lg:mx-4 motion-safe:hover:scale-105"
                >
                  Vêtements
                </a>
                <a
                  href="#"
                  className="mt-2 text-black duration-300 transform lg:mt-0 lg:mx-4 motion-safe:hover:scale-105"
                >
                  Objets
                </a>
                <a
                  href="#"
                  className="mt-2 text-black duration-300 transform lg:mt-0 lg:mx-4 motion-safe:hover:scale-105"
                >
                  Personnalisation
                </a>
                <a
                  href="#"
                  className="mt-2 text-black duration-300 transform lg:mt-0 lg:mx-4 motion-safe:hover:scale-105"
                >
                  à Propos
                </a>
              </div>

              <div className="flex justify-center items-center mt-6 lg:flex lg:mt-0 lg:-mx-2">
                <a
                  href={'https://www.instagram.com/sarahjane.clothes/'}
                  className="motion-safe:hover:scale-105"
                >
                  <Image
                    width={24}
                    height={24}
                    src={'/insta.png'}
                    alt="Instagram"
                  />
                </a>

                <div>
                  <button
                    aria-label="toggle-dark-mode"
                    className="p-2 m-2 "
                    onClick={() => customToggleDarkMode()}
                  >
                    Change color mode
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default CustomHeader
