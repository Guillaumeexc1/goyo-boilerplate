import { FC } from 'react'
import Image from 'next/image'
import { darkMode } from '../pages/_app'
import { useAtom } from 'jotai'

const CustomFooter: FC = () => {
  const [isDarkMode] = useAtom(darkMode)

  return (
    <footer className={`${isDarkMode && 'dark'}`}>
      <div className="bg-primary-100 dark:bg-primary-300">
        <hr className="h-px mx-20 bg-primary-300 border-none dark:bg-primary-100" />
        <div className="container p-6 mx-auto z-10">
          <div className="text-center pb-10">
            <Image
              src="/sarahjanename.png"
              alt="Sarah Jane"
              width={300}
              height={50}
            />
            <div className="mb-2 text-primary-300 dark:text-primary-100">
              Follow me
            </div>
            <div className="flex justify-center gap-2 text-primary-300 dark:text-primary-100">
              <Image src="/insta.png" alt="Instagram" width={28} height={28} />
              sarahjane.clothes
            </div>
          </div>

          <div className="lg:flex">
            <div className="mt-2 lg:mt-0 lg:flex-1">
              <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 text-center">
                <div>
                  <h3 className=" uppercase text-primary-300 dark:text-primary-100 ">
                    Service client
                  </h3>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    Guide des tailles
                  </a>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    FAQ
                  </a>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    Nous contacter
                  </a>
                </div>

                <div>
                  <h3 className=" uppercase text-primary-300 dark:text-primary-100">
                    à propos
                  </h3>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    L&apos;Atelier
                  </a>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    La créatrice
                  </a>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    Le savoir faire
                  </a>
                  <a
                    href="#"
                    className="block dark:text-primary-200 mt-2 text-sm duration-500 motion-safe:hover:scale-105"
                  >
                    Vos avis
                  </a>
                </div>
              </div>
            </div>
          </div>

          <hr className="h-px my-6 bg-primary-300 dark:bg-primary-100 border-none" />

          <div>
            <p className="text-center text-primary-300 dark:text-primary-100">
              © Sarah Jane 2022 - All rights reserved
            </p>
          </div>
        </div>
        {/*
      <div
        style={{
          position: 'absolute',
          width: '100%',
          height: '300px',
          marginTop: '-300px',
          opacity: '0.7',
          zIndex: 1
        }}
      >
        <Image
          alt="Mountains"
          src={'/fondsansfondtest.png'}
          layout="fill"
          objectFit="contain"
        />
      </div>*/}
      </div>
    </footer>
  )
}

export default CustomFooter
