import { FC } from 'react'

const Loading: FC = () => {
  return (
    <div className="container mx-auto flex flex-col items-center justify-center min-h-screen p-4 gap-10">
      <h1 className="text-2xl md:text-[5rem] leading-normal font-extrabold">
        Loading ...
      </h1>
    </div>
  )
}

export default Loading
