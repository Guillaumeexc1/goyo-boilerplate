// src/server/router/index.ts

import { createRouter } from './context'
import { exampleRouter } from './default-example'
import { protectedExampleRouter } from './protected-example-router'
import superjson from 'superjson'

export const appRouter = createRouter()
  .transformer(superjson)
  .merge('default.', exampleRouter)
  .merge('question.', protectedExampleRouter)

// export type definition of API
export type AppRouter = typeof appRouter
