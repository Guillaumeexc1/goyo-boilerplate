import { createRouter } from './context'

// Example router with queries that can only be hit if the user requesting is signed in
export const exampleRouter = createRouter().query('getPublicMessage', {
  resolve({ ctx }) {
    return "Je ne meurs pas evangelyn, j'entre dans la légende."
  }
})
