/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin')
const backfaceVisibility = plugin(function ({ addUtilities }) {
  addUtilities({
    '.backface-visible': {
      'backface-visibility': 'visible'
    },
    '.backface-hidden': {
      'backface-visibility': 'hidden',
      transform: 'translateZ(0)',
      '-webkit-font-smoothing': 'subpixel-antialiased'
    }
  })
})

module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#f8f0cb',
          200: '#f8cd7e',
          300: '#671725'
        },
        secondary: {
          100: '#671725',
          200: '#f8cd7e',
          300: '#f8f0cb'
        }
      }
    }
  },
  plugins: [backfaceVisibility]
}
